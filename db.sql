-- --------------------------------------------------------
-- Host:                         inventage.co
-- Server version:               5.5.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zazzifbv_ss
CREATE DATABASE IF NOT EXISTS `zazzifbv_ss` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zazzifbv_ss`;

-- Dumping structure for table zazzifbv_ss.indexui
CREATE TABLE IF NOT EXISTS `indexui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `text3` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table zazzifbv_ss.indexui: ~1 rows (approximately)
/*!40000 ALTER TABLE `indexui` DISABLE KEYS */;
INSERT INTO `indexui` (`id`, `text1`, `text2`, `text3`) VALUES
	(1, 'Super sign', '201 หมู่ที่6 ตำบลในคลองบางปลากด อำเภอพระสมุทรเจดีย์ จังหวัดสมุทรปราการ 10290   \nอิเมล aodza1113@hotmail.com                                                                 \nไลน์ aotfd', ' ');
/*!40000 ALTER TABLE `indexui` ENABLE KEYS */;

-- Dumping structure for table zazzifbv_ss.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT 'เลขออเดอร์',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ไอดีที่สั่งซื้อ',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT 'รูปสินค้าของระบบซื้อของหน้าเว็บ',
  `payment_date` varchar(50) NOT NULL COMMENT 'วันที่ การสั่งซื้อ',
  `design_picture` text NOT NULL COMMENT 'รูปสินค้าที่ออกแบบ  > ของออกแบบ',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT 'ความกว้างของแผ่นป้าย > ของออกแบบ',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT 'ความยาวของแผ่นป้าย > ของออกแบบ',
  `materialtype` varchar(30) NOT NULL DEFAULT '0' COMMENT 'วัสถุที่ใช้ทำแผ่นป้าย > ของออกแบบ',
  `user_address` text NOT NULL COMMENT 'ที่อยู่ผู้รับ',
  `message` text NOT NULL COMMENT 'ข้อความบนป้าย',
  `quantity_payment` int(11) NOT NULL DEFAULT '0' COMMENT 'จำนวน',
  `detail` text NOT NULL,
  `production_price` int(11) NOT NULL DEFAULT '0',
  `status` varchar(30) NOT NULL COMMENT 'สถานะการสั่งซื้อ 1.รอประเมิณ 2โอนเงิน. 3.รอตรวจสอบ 4.จัดส่ง 5.เรียบร้อย',
  `type_p` varchar(30) NOT NULL COMMENT '1=สั่งซื้อหน้าบ้าน // 2.สั่งซื้อแบบออกแบบ',
  `premise` text NOT NULL COMMENT 'ใบเสร็จ',
  `read` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- Dumping data for table zazzifbv_ss.payment: ~30 rows (approximately)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;

-- Dumping structure for table zazzifbv_ss.storage
CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` text NOT NULL,
  `date_product` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `details` text NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `type` varchar(100) NOT NULL DEFAULT '',
  `quantity` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Dumping data for table zazzifbv_ss.storage: ~27 rows (approximately)
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
INSERT INTO `storage` (`id`, `picture`, `date_product`, `name`, `details`, `price`, `type`, `quantity`) VALUES
	(35, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-09_12%3A04%3A00_cfaa4b2ce1d936f0a30468d879b9769d/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%9B%E0%B8%B4%E0%B8%94-%E0%B8%9B%E0%B8%B4%E0%B8%94.jpg', '09/03/2020', 'ป้ายเปิด-ปิด', 'มีขนาด กว้าง 15 ซม. x สูง 10 ซม.', 1300, '1', 20),
	(36, 'https://super-sign.s3.amazonaws.com/images/2020-03-10_08%3A15%3A53_8dd9018e37cc4d6b64d49d1a11af1f86/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%82%E0%B8%A3%E0%B8%87%E0%B9%80%E0%B8%A3%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B9%84%E0%B8%A1%E0%B9%89.jpg', '10/03/2020', 'ป้ายโรงเรียนไม้', 'มีขนาดกว้าง 25 ซม. ยาว 80 ซม.  ด้ามจับยาว 150 ซม.', 1350, '1', 20),
	(37, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A16%3A55_ee48e78965b318ad73f2874434c76f41/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%99%E0%B9%89%E0%B8%B3.jpg', '10/03/2020', 'ป้ายห้องน้ำ', 'มีขนาด กว้าง 40 ซม. ยาว 60 ซม.', 740, '1', 10),
	(38, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A18%3A01_972d72f47192470d5525a46de7578882/%E0%B8%9E%E0%B8%A7%E0%B8%81%E0%B8%81%E0%B8%B8%E0%B8%8D%E0%B9%81%E0%B8%88%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%9E%E0%B8%B1%E0%B8%81.jpg', '10/03/2020', 'พวกกุญแจห้องพัก', 'มีขนาดกว้าง 5 ซม. ยาว 10 ซม.', 120, '1', 20),
	(39, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A18%3A45_0109136c4e0cdae4e679b67d8cfe4ab0/%E0%B8%9E%E0%B8%A7%E0%B8%87%E0%B8%81%E0%B8%B8%E0%B8%8D%E0%B9%81%E0%B8%88%E0%B9%84%E0%B8%A1%E0%B9%89.jpg', '10/03/2020', 'พวงกุญแจไม้', 'มีขนาดกว้าง 15 ซม. ยาว 20 ซม.', 180, '1', 25),
	(40, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A27%3A20_7d69583c987db8783e04be1ae699430d/%E0%B9%80%E0%B8%A1%E0%B8%99%E0%B8%B9%E0%B9%84%E0%B8%A1%E0%B9%89.jpg', '10/03/2020', 'เมนู', 'มีความกว้าง 15 ยาว 30 ซม.', 650, '1', 20),
	(41, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A33%3A24_5370a528a860e48d986fc99f766d7e82/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%81%E0%B8%82%E0%B8%A7%E0%B8%99%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87.png', '10/03/2020', 'ป้ายแขวนหน้าห้อง', 'มีขนาดกว้าง 15 ซม ยาว 45 ซม.', 270, '1', 20),
	(42, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_08%3A43%3A40_220cedf0cd9c587c3088c326c281b480/%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B8%AD%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B8%A3%E0%B9%84%E0%B8%A1%E0%B9%89.jpg', '10/03/2020', 'ตัวอักษรไม้', 'มีขนาดกว้าง 15 ซม. ยาว 10 ซม.', 400, '1', 30),
	(43, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A14%3A00_5c62bacb7c39ce815663808086aa6b66/%E0%B8%95%E0%B8%81%E0%B9%81%E0%B8%95%E0%B9%88%E0%B8%87%E0%B8%A0%E0%B8%B2%E0%B8%A2%E0%B9%83%E0%B8%99.jpg', '10/03/2020', 'ตัวอักษรแบบตกแต่งภายใน', 'มีขนาด กว้าง 30 ซม. ยาว 45 ซม.', 800, '4', 100),
	(44, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A26%3A45_35d551162de3f4bd487ceb07057bd656/%E0%B8%95%E0%B8%81%E0%B9%81%E0%B8%95%E0%B9%88%E0%B8%87%E0%B8%A3%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99.jpg', '10/03/2020', 'ตกแต่งรั่วบ้าน', 'มีขนาดกว้าง 120 ซม. ยาว 150 ซม.', 2500, '4', 100),
	(45, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A34%3A26_545b6f6b2ef8de21d5c66f7fdb9692e0/%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B9%80%E0%B8%A5%E0%B8%82.jpg', '10/03/2020', 'ตัวเลข', 'มีขนาดกว้าง 25 ซม. ยาว 38 ซม.', 490, '1', 100),
	(46, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A39%3A23_526f81e1fb2f4552722be3e3832c92a4/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%AA%E0%B8%B5%E0%B9%80%E0%B8%AB%E0%B8%A5%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A1%E0%B8%9E%E0%B8%B7%E0%B9%89%E0%B8%99%E0%B8%9C%E0%B9%89%E0%B8%B2.jpg', '10/03/2020', 'ป้ายชื่อร้าน', 'มีขนาดกว้าง 80ซม. ยาว 140 ซม.', 2700, '4', 5),
	(47, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A41%3A38_3540bbf07d168da92cecf0fac5e00735/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%AB%E0%B8%A5%E0%B9%87%E0%B8%81%E0%B8%89%E0%B8%A5%E0%B8%B8.jpg', '10/03/2020', 'ป้ายเหล็กฉลุ', 'มีขนาด 120 ซม. ยาว 170 ซม.', 2400, '4', 10),
	(48, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A43%3A14_76391439890d20b364c6f6333ad98d16/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%AA%E0%B8%B5%E0%B9%80%E0%B8%AB%E0%B8%A5%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A1%E0%B8%9E%E0%B8%B7%E0%B9%89%E0%B8%99%E0%B8%9C%E0%B9%89%E0%B8%B2.jpg', '10/03/2020', 'ป้ายชื่อร้าน', 'มีขนาดกว้าง 80ซม. ยาว 140 ซม.', 2700, '4', 5),
	(49, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A43%3A23_53d343bd977629e3b845e250677c7919/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%AD%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B8%A3%E0%B9%84%E0%B8%9F%E0%B8%AD%E0%B8%AD%E0%B8%81%E0%B8%AB%E0%B8%A5%E0%B8%B1%E0%B8%87.jpg', '10/03/2020', 'ป้ายอักษรไฟออกหลัง', 'มีขนาด กว้าง 100 ซม. ยาว 140 ซม.', 3500, '4', 10),
	(50, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A44%3A31_c44becae1eef057bee84520c860cfd8f/%E0%B8%A7%E0%B8%B1%E0%B8%99%20%E0%B9%80%E0%B8%94%E0%B8%B7%E0%B8%AD%E0%B8%99.jpg', '10/03/2020', 'ป้ายแบบตั้งโต๊ะ', 'มีขนาด กว้าง 21 ซม. ยาว 30 ซม.', 480, '4', 10),
	(51, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A46%3A01_790a2a92c89025d3896fc836b6c606d0/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD.jpg', '10/03/2020', 'ป้ายชื่อ', 'มีขนาด กว้าง 1 ซม. ยาว 2 ซม.', 120, '3', 100),
	(52, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A48%3A22_819ef98dcfa1eaa889e2f075c4c7b734/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%AD%E0%B8%81.jpg', '10/03/2020', 'ป้ายชื่อแบบใส', 'มีขนาด กว้าง 1 ซม  ยาว 2 ซม.', 150, '3', 100),
	(53, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A49%3A30_e19ce0dbf42e366c4d12751cf856cd9b/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B9%80%E0%B8%A5%E0%B8%82.jpg', '10/03/2020', 'ป้ายติดผนัง', 'มีขนาด 10 ซม. ยาว 20 ซม.', 750, '3', 10),
	(54, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A50%3A21_6bdea0091a3a68a494576749c1ddeba5/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B8%AD%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B8%A3%E0%B9%82%E0%B8%A5%E0%B8%AB%E0%B8%B0.jpg', '10/03/2020', 'ตัวอักษร', 'มีขนาด กว้าง 20 ซม. ยาว 50 ซม.', 950, '3', 15),
	(55, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A51%3A42_41322fee59251b6f9b26790c29196246/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87.jpg', '10/03/2020', 'ป้ายชื่อติดผนัง', 'มีขนาดกว้าง 15 ซม. ยาว 30 ซม.', 480, '3', 20),
	(56, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A52%3A51_a8020d0df26b6ae7acd8d9f951c7e7b6/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%97%E0%B8%B2%E0%B8%87%E0%B8%AB%E0%B8%99%E0%B8%B5%E0%B9%84%E0%B8%9F.jpg', '10/03/2020', 'ป้ายทางหนีไฟ', 'มีขนาดกว้าง 70 ซม. ยาว 120 ซม.', 3400, '3', 5),
	(57, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_09%3A59%3A46_7a36b0722e42a50e9c8d82c4312dac9e/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0.jpg', '10/03/2020', 'ป้ายแบบตั้งโต๊ะ', 'มีขนาด กว้าง 15 ซม. ยาว 25 ซม.', 750, '3', 100),
	(58, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_10%3A01%3A38_ebe4145db1ef571ef63b71571ae87c5f/%E0%B8%81%E0%B8%A5%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%AA%E0%B9%88%E0%B8%82%E0%B8%AD%E0%B8%87.jpg', '10/03/2020', 'กล่องใส่ของ', 'มีขนาด กว้าง 40 ซม. ยาว 70 ซม.', 1400, '2', 10),
	(59, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_10%3A05%3A59_52da6b7559444e905c9400bc7ac4596e/%E0%B8%8A%E0%B8%B1%E0%B9%89%E0%B8%99%E0%B8%A7%E0%B8%B2%E0%B8%87%E0%B9%82%E0%B8%9B%E0%B8%A3%E0%B8%8A%E0%B8%B1%E0%B8%A7%E0%B8%A3%E0%B9%8C%20%E0%B8%82%E0%B8%99%E0%B8%B2%E0%B8%94%20A3%20%E0%B8%AA%E0%B8%B9%E0%B8%87%20150%E0%B8%8B%E0%B8%A1%2035%E0%B8%8456.jpg', '10/03/2020', 'ชั้นวางโปรชัวร์ ขนาด a3', 'มีขนาดกว้าง 36 ซม. ยาว 56 ซม. สูง 150 ซม.', 1800, '2', 10),
	(61, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_10%3A21%3A47_733439ac0b26225c3aab71a65a94619b/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%A7%E0%B8%B2%E0%B8%87%E0%B9%82%E0%B8%97%E0%B8%A3%E0%B8%A8%E0%B8%B1%E0%B8%9E%E0%B8%97%E0%B9%8C.jpg', '10/03/2020', 'ที่วางโทรศัพท์', 'มีขนาดกว้าง 10 ซม. ยาว 15 ซม.', 180, '2', 10),
	(64, 'https://super-sign.s3.ap-southeast-1.amazonaws.com/images/2020-03-10_10%3A24%3A43_277ba6642ac33f9dcc360c68a39d81dc/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%AD%E0%B8%81.jpg', '10/03/2020', 'ป้ายชื่อแบบติดหน้าอก', 'มีขนาด 2 ซม. ยาว 3 ซม.', 120, '2', 100);
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;

-- Dumping structure for table zazzifbv_ss.type_product
CREATE TABLE IF NOT EXISTS `type_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_pd` varchar(50) DEFAULT NULL,
  `status_pd` int(11) DEFAULT '1' COMMENT '1= ใช้อยู่ // 2. ไม่ใช้แล้ว',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table zazzifbv_ss.type_product: ~5 rows (approximately)
/*!40000 ALTER TABLE `type_product` DISABLE KEYS */;
INSERT INTO `type_product` (`id`, `type_pd`, `status_pd`) VALUES
	(1, 'ไม้', 1),
	(2, 'อะคริลิค', 1),
	(3, 'อะลูมิเนียม', 1),
	(4, 'เหล็ก', 1),
	(20, 'ทดสอบ', 2);
/*!40000 ALTER TABLE `type_product` ENABLE KEYS */;

-- Dumping structure for table zazzifbv_ss.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Dumping data for table zazzifbv_ss.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `firstname`, `lastname`, `email`, `tel`, `address`, `status`) VALUES
	(4, 'admin', 'admin', 'new2', 'fuke', 'art1@hotmail.com', '01', '1', 'admin'),
	(5, 'tan', '1234', 'apichot', 'patcharat', 'apichot_5068@gmail.com', '0929484050', 'home', 'admin'),
	(58, 'aotza1113', '123456', 'อนุชา', 'สมนึกแท่น', 'aotza1113@hotmail.com', '095-212-7146', '201 เทสระบบ ครั้งที่1', ''),
	(59, 'big', 'big1234', '', '', '', '', '', '');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
