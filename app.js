//---- นำเข้า Dependency ต่างๆ ----//
const express = require('express')
const fs = require('fs');
const AWS = require('aws-sdk');

//---- ตั่งค่า Dependency และ ตัวแปลเริ่มต้น ----//
const app = express()
app.use(express.json()) // FIX: การรับ body เป็น Json

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.setHeader('Access-Control-Allow-Methods', "*")
  res.header("Access-Control-Allow-Headers","*")
  next()
}); // FIX: การส่ง Rest ที่ต้องการเชคค่า Header ก่อน (จะส่งผ่าน OPTIONS ก่อน)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


app.use('/user', require('./src/user'))
app.use('/storage', require('./src/storage'))
app.use('/payment', require('./src/payment'))
app.use('/indexui', require('./src/indexui'))
// app.use('/type_material', require('./src/type_material'))

app.all('/', (req, res) => { res.send("ok") })
app.all('*', (req, res) => { res.send(404) })


app.listen(process.env.PORT || 8080, () => console.log(`Listening on port${process.env.PORT || 8080}...`))