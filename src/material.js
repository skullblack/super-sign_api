const db = require('../global/mysql_db')
const mw = require('../global/middleware')

// module.exports = (app) => {
const express = require('express')
const route = express.Router()

//เรียกดูวัสดุ
route.get('/', (req, res) => {
  let sql = 'SELECT * FROM type_product'
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//เพิ่มวัสถุ 
route.put('/', mw.material_check, (req, res) => {
  let sql = `
      INSERT INTO type_product (              
        type_pd
      )
      VALUES (
        ${db.escape(req.body.material || "")}
      )
      `
  db.query(sql)
  res.send(200)
})

// แก้ไข วัสดุ
// route.post('/', (req, res) => {
//   let sql = 
//     `
//       UPDATE type_material SET
//         material = ${db.escape(req.body.material || "")},
//         price_weight = ${db.escape(req.body.price_weight || "")}
//       WHERE id = ${req.body.id}
//     `
//   db.query(sql)
//   res.send(200)
// })

// ลบ วัสดุ
// route.delete('/:id', (req, res) => {
//   let sql = 
//       `
//         DELETE FROM type_material WHERE id = ${req.params.id};
//       `
//   db.query(sql)
//   res.send(sql)
// })

// }
module.exports = route



