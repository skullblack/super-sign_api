const db = require('../global/mysql_db')
const mw = require('../global/middleware')

const express = require('express')
const route = express.Router()

route.get('/', (req, res) => {
  let sql = `SELECT * FROM indexui`
  db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

route.post('/alltext', (req, res) => {
  let sql = `
      UPDATE indexui SET
        text1 = ${db.escape(req.body.text1 || "")},
        text2 = ${db.escape(req.body.text2 || "")},
        text3 = ${db.escape(req.body.text3 || "")}
      WHERE id = 1
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

module.exports = route