//---- นำเข้า Dependency ต่างๆ ----//
const db = require('../global/mysql_db')
const multer = require('multer')
const fs = require('fs.extra')
const AWS = require('aws-sdk');
const uuid = require('uuid/v4')
const moment = require('moment')
const express = require('express')
const mw = require('../global/middleware')
const route = express.Router()
// module.exports = (app) => {

//แสดงสินค้า2ชิ้นล่าสุด
route.get('/s2', (req, res) => {
  let sql = `
      SELECT * FROM storage
      GROUP BY id
      ORDER BY id DESC LIMIT 4;
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

// single product
route.get('/single/:id', (req, res) => {
  let sql = `
    SELECT * FROM storage WHERE id = ${req.params.id}
  `
  db.query(sql, (err, results) => {
    if (!err) {
      res.json(results)
    } else {
      console.log(err)
      res.status(503).send(err)
    }
  })
})

//ประเภทสินค้า
route.get('/pd', (req, res) => {
  let sql = `
      SELECT storage.*, type_product.type_pd FROM storage
        INNER JOIN type_product
        ON type_product.id = storage.type
    `

  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
}) 

route.get('/type', (req, res) => {
  let sql = `SELECT * FROM type_product WHERE status_pd = 1`
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

// route.get('/type_product', (req, res) => {
//   let sql = `SELECT * FROM type_product`
//   db.query(sql, (err, results) => {
//     if (err) throw err
//     res.json(results)
//   })
// })

//เพิ่มวัสถุ ห้ามลบ ใช้
route.put('/type_product', mw.material_check, (req, res) => {
  let sql = `
      INSERT INTO type_product (              
        type_pd
      )
      VALUES (
        ${db.escape(req.body.material || "")}
      )
      `
      db.query(sql, (err, results) => {
        if (!err) {
          res.send(200)
        } else {
          console.log(err)
          res.status(503).send(err)
        }
      })
})

// ดูคลังสินค้า กูใช้แล้วห้ามลบ
route.get('/', (req, res) => {
  let sql = `
      SELECT storage.*,type_product.id AS type_id, type_product.type_pd FROM storage
      INNER JOIN type_product
      ON type_product.id = storage.type
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//---- อัพคลังสินค้าพร้อมรูปภาพ ----//
route.put('/', multer({ dest: `${__dirname}/../upload/` }).single('file'), (req, res) => {
  const s3 = new AWS.S3({
    accessKeyId: 'AKIAJPW5BNRJ4IRKSAMQ',
    secretAccessKey: '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'
  });
  const fileContent = fs.readFileSync(req.file.path)
  // Setting up S3 upload parameters
  const params = {
    Bucket: 'super-sign',
    Key: `images/${moment().format('YYYY-MM-DD_HH:mm:ss')}_${req.file.filename}/${req.file.originalname}`, // File name you want to save as in S3
    Body: fileContent,
    ACL: 'public-read'
  };
  // Uploading files to the bucket
  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err)
      res.send(503)
    } else {
      // console.log(`File uploaded successfully. ${data.Location}`)
      let sql = `
            INSERT INTO storage(
              picture,
              name,
              details,
              price,
              type,
              date_product,
              quantity)
            VALUES (
              '${data.Location || ""}',
              ${db.escape(req.body.name || "")},
              ${db.escape(req.body.details || "")},
              ${db.escape(req.body.price || "")},
              ${db.escape(req.body.type || "")},
              ${db.escape(moment().format('DD/MM/YYYY') || "")},
              ${db.escape(req.body.quantity || "")}
            )
          `
      db.query(sql)
      res.send(200)
    }
  });
});

// แก้ไขสินค้า
route.post('/', (req, res) => {
  let sql = `
      UPDATE storage SET
        name = ${db.escape(req.body.name || "")},
        details = ${db.escape(req.body.details || "")},
        price = ${db.escape(req.body.price || "")},
        quantity = ${db.escape(req.body.quantity || "")}
      WHERE id = ${req.body.id};
      `
  db.query(sql)
  res.send(200)
})

//ลบสินค้า
route.delete('/:id', (req, res) => {
  let sql = `
      DELETE FROM storage WHERE id = ${req.params.id};
    `
  db.query(sql)
  res.send(200)
})
// }
////////////////////////////////////////////////////// 
// material
//เพิ่ม material
route.put('/add_material',mw.user_check, (req, res) => {
  let sql = `
      INSERT INTO type_product (
        type_pd,
        status_pd
      )
      VALUES (
        ${db.escape(req.body.type_pd || "")},
       1
      )
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

//แก้ไข material
route.post('/edit_material', (req, res) => {
  let sql = `
      UPDATE type_product SET
        type_pd = ${db.escape(req.body.type_pd || "")}
      WHERE id = ${req.body.id}
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

//ลบ material แก้ไขสถานะ
route.post('/delete_material', (req, res) => {
  let sql = `
      UPDATE type_product SET
        status_pd = 2
      WHERE id = ${req.body.id}
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})
module.exports = route