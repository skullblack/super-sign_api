const db = require('../global/mysql_db')
const mw = require('../global/middleware')

const express = require('express')
const route = express.Router()

// module.exports = (route) => {

// แสดงข้อมูล user ที่ต้องการ
route.get('/checkuser/:id', (req, res) => {
  let sql = `SELECT * FROM user WHERE id = ${req.params.id};`
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results[0])
  })
})

//user_check
route.get('/check', (req, res) => {
  let sql = `
      SELECT * FROM user
      WHERE username = ${db.escape(req.headers.username)} 
      AND password = ${db.escape(req.headers.password)}
    `
  db.query(sql, (err, results) => {
    if (!err) {
      if (results.length > 0) {
        res.json(results[0])
      } else {
        res.send(401)
      }
    } else {
      console.log(err)
      res.status(503).send(err)
    }
  })
})

//แสดงข้อมูล user ที่ต้องการ เฉพาะแอดมิน
route.get('/:id', mw.admin, (req, res) => {
  let sql = `SELECT * FROM user WHERE id = ${req.params.id};`
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results[0])
  })
})

// แสดงข้อมูล user ทั้งหมด เฉพาะแอดมิน
route.get('/', mw.admin, (req, res) => {
  let sql = `SELECT * FROM user WHERE username != '${req.headers.username}'`
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

// เพิ่ม user
route.put('/',mw.user_check, (req, res) => {
  let sql = `
      INSERT INTO user (
        username,
        password,
        firstname,
        lastname,
        email,
        tel,
        address
      )
      VALUES (
        ${db.escape(req.body.username || "")},
        ${db.escape(req.body.password || "")},
        ${db.escape(req.body.firstname || "")},
        ${db.escape(req.body.lastname || "")},
        ${db.escape(req.body.email || "")},
        ${db.escape(req.body.tel || "")},
        ${db.escape(req.body.address || "")}
      )
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

// แก้ไข user
route.post('/', (req, res) => {
  let sql = `
      UPDATE user SET
        username = ${db.escape(req.body.username || "")},
        password = ${db.escape(req.body.password || "")},
        firstname = ${db.escape(req.body.firstname || "")},
        lastname = ${db.escape(req.body.lastname || "")},
        email = ${db.escape(req.body.email || "")},
        tel = ${db.escape(req.body.tel || "")},
        address = ${db.escape(req.body.address || "")}
      WHERE id = ${req.body.id}
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

// ลบ user
route.delete('/:id', (req, res) => {
  let sql = `
      DELETE FROM user WHERE id = ${req.params.id};
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

// }

module.exports = route