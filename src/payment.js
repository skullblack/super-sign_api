const db = require('../global/mysql_db')
const multer = require('multer')
const fs = require('fs.extra')
const AWS = require('aws-sdk');
const uuid = require('uuid/v4')
const moment = require('moment')
const express = require('express')
const route = express.Router()

//เรียกดูออเดอร์ของ user นั้น
route.get('/order_list/user_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT storage.*, payment.*,
    SUM(storage.price*payment.quantity_payment) AS sumtotal, SUM(payment.quantity_payment) AS allquantity,payment.id AS payment_id
      FROM payment 
        LEFT JOIN storage 
        ON storage.id = payment.product_id 
      WHERE payment.user_id = ${req.params.id} GROUP BY order_id;
  `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//เรียกดูออเดอร์ของที่มีสเตตัส3
route.get('/order_list/all_order', (req, res) => {
  let sql =
    ` 
    SELECT storage.*, payment.*,
    SUM(storage.price*payment.quantity_payment) AS sumtotal, SUM(payment.quantity_payment) AS allquantity,payment.id AS payment_id
      FROM payment 
        LEFT JOIN storage 
        ON storage.id = payment.product_id 
       GROUP BY order_id;
  `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})


//เรียกดูข้อมูลของออเดอร์นั้น ตาม เลขไอดี
route.get('/order_list/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment
      INNER JOIN storage
      ON payment.product_id = storage.id  
      WHERE payment.order_id = ${req.params.id}
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})

route.get('/order_list/order_id_production/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment
    WHERE order_id = ${req.params.id}
    `
    db.query(sql, (err, results) => {
      if (!err) {
        res.json(results)
      } else {
        console.log(err)
        res.status(503).send(err)
      }
  })
})
//เรียกดูข้อมูลของออเดอร์นั้น ตาม เลขไอดี production
route.get('/order_list_production/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment
      WHERE order_id = ${req.params.id}
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//เรียกดูออเดอร์ที่เป็นสเตตัส3
route.get('/check_status3/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment 
      INNER JOIN storage
      ON payment.product_id = storage.id  
      WHERE payment.order_id = ${req.params.id} AND payment.status = '3'
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    console.log(results,sql);

    res.json(results)
  })
})
//เรียกดูออเดอร์ที่เป็นสเตตัส3 production
route.get('/check_status3_production/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment  
      WHERE order_id = ${req.params.id} AND status = '3'
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    console.log(results,sql);

    res.json(results)
  })
})

// admin เรียกดู payment 
route.get('/view_payment/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment 
      INNER JOIN storage
      ON payment.product_id = storage.id
      INNER JOIN user
      ON user.id = payment.user_id
      WHERE payment.order_id = ${req.params.id} 
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})
// admin เรียกดู payment production
route.get('/view_payment_production/order_id/:id', (req, res) => {
  let sql =
    ` 
    SELECT * FROM payment 
      INNER JOIN user
      ON user.id = payment.user_id
      WHERE payment.order_id = ${req.params.id} 
    `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//เปลี่ยนสถานะpayment = 6 คือเรียบร้อย
route.post('/delete', (req, res) => {
  let sql = `
      UPDATE payment SET
        status = '6'
      WHERE order_id = ${req.body.order_id}
      `
  db.query(sql)
  res.send(200)
})

//เปลี่ยนสถานะpayment = 3 และอัพรูปบิล
route.post('/up_status3', multer({ dest: `${__dirname}/../upload/` }).single('file'), (req, res) => {
  const s3 = new AWS.S3({
    accessKeyId: 'AKIAJPW5BNRJ4IRKSAMQ',
    secretAccessKey: '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'
  });
  const fileContent = fs.readFileSync(req.file.path)
  // Setting up S3 upload parameters
  const params = {
    Bucket: 'super-sign',
    Key: `images/${moment().format('YYYY-MM-DD_HH:mm:ss')}_${req.file.filename}/${req.file.originalname}`, // File name you want to save as in S3
    Body: fileContent,
    ACL: 'public-read'
  };
  // Uploading files to the bucket
  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err)
      res.send(503)
    } else {
      let sql = `
        UPDATE payment SET
          status = '3',
          premise =  '${data.Location || ""}'
        WHERE order_id = ${req.body.order_id};
      `
      db.query(sql)
      res.sendStatus(200)
    }
  });
})

//เปลี่ยนสถานะpayment = 4
route.post('/up_status4', (req, res) => {
  let sql = `
      UPDATE payment SET
        status = '4',
        payment_date = ${db.escape(moment().format('DD/MM/YYYY,HH:mm:ss') || "")}
      WHERE order_id = ${req.body.order_id}
      `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//เปลี่ยนสถานะpayment = 5
route.post('/up_status5', (req, res) => {
  let sql = `
      UPDATE payment SET
        status = '5'
      WHERE order_id = ${req.body.order_id}
      `
  db.query(sql)
  res.send(200)
})

//อัพราคาออกแบบและเปลี่ยนสถานะ 
route.post('/up_production_price', (req, res) => {
  let sql = `
      UPDATE payment SET
        production_price = ${db.escape(req.body.price || "")},
        status = ${db.escape(req.body.status || "")}
      WHERE order_id = ${req.body.order_id}
      `
      // console.log(req.body,sql);
      db.query(sql, (err, results) => {
        if (!err) {
          res.json(results)
        } else {
          console.log(err)
          res.status(503).send(err)
        }
    })
})


//แสดงออเดอร์ทั้งหมด
route.get('/', (req, res) => {
  let sql = `
      SELECT * FROM storage
      GROUP BY id 
      ORDER BY id DESC LIMIT 2;
    `
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//แสดงข้อมูลของแต่ละออเดอร์ 
route.get('/:id', (req, res) => {
  let sql = `
      SELECT payment.*, storage.picture,storage.name,storage.type FROM payment
      left JOIN storage
      ON payment.product_id = storage.id
      WHERE payment.order_id = ${req.params.id}
    `
  let query = db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

// เพิ่มออเดอร์เวลาซื้อสินค้าจากหน้าร้าน และ ออกแบบสินค้า
route.put('/', multer({ dest: `${__dirname}/../upload/` }).single('file'), (req, res) => {
  db.query(`SELECT  MAX(order_id) as max FROM payment`, (err, results) => {
    let max = results[0].max;
    var order_date = moment().format('DD/MM/YYYY,HH:mm:ss');
    if (req.body.type_p == 1) {
      let sql = req.body.carts.map(cart => `
      (
        ${db.escape(max + 1 || "")},
        ${db.escape(order_date || "")},
        ${db.escape(req.body.id_user || "")},
        ${db.escape(cart.id || "")},
        ${db.escape(req.body.address || "")},
        ${db.escape(cart.message || "")},
        ${db.escape(cart.count || "")},
        '2',
        ${db.escape(req.body.type_p || "")}
      )
    `)
      if (sql.length <= 0) {
        res.status(503).send("No shift")
      } else {
        sql = `
        INSERT INTO payment (
          order_id,
          order_date,
          user_id,
          product_id,
          user_address,
          message,
          quantity_payment,
          status,
          type_p
        ) VALUE ${sql.join(",")}
      `
        db.query(sql, (err, results) => {
          if (!err) {
            res.send({ order_id: max + 1 })
          } else {
            console.log(err)
            res.status(503).send(err)
          }
        })
      }
    } else if (req.body.type_p == 2) {
      const s3 = new AWS.S3({
        accessKeyId: 'AKIAJPW5BNRJ4IRKSAMQ',
        secretAccessKey: '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'
      });
      const fileContent = fs.readFileSync(req.file.path)
      // Setting up S3 upload parameters
      const params = {
        Bucket: 'super-sign',
        Key: `images/${moment().format('YYYY-MM-DD_HH:mm:ss')}_${req.file.filename}/${req.file.originalname}`,
        Body: fileContent,
        ACL: 'public-read'
      };
      s3.upload(params, function (err, data) {
        if (err) {
          console.log(err)
          res.send(503)
        } else {
          // console.log(`File uploaded successfully. ${data.Location}`)
          let sql = `
                INSERT INTO payment(
                  order_id,
                  order_date,
                  user_id,
                  design_picture,
                  width,
                  height,
                  materialtype,
                  quantity_payment,
                  detail,
                  user_address,
                  status,
                  type_p)
                VALUES (
                  ${Number(max) + 1},
                  ${db.escape(order_date || "")},
                  ${db.escape(req.body.user_id || "")},
                  '${data.Location}',
                  ${db.escape(req.body.width || "")},
                  ${db.escape(req.body.height || "")},
                  ${db.escape(req.body.materialtype || "")},
                  ${db.escape(req.body.quantity || "")},
                  ${db.escape(req.body.detail || "")},
                  ${db.escape(req.body.address || "")},
                  '1',
                  ${req.body.type_p || ""});
              `
          db.query(sql)
          res.sendStatus(200)
        }
      })
    } else {
      console.log('type_error')
      res.status(503).send('type_error')
    }
  })
})

route.post('/up_readall', (req, res) => {
  let sql = `
    UPDATE payment SET 
      \`read\`= 2 
      `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

route.post('/up_read', (req, res) => {
  let sql = `
    UPDATE payment SET 
      \`read\`= 2 
    WHERE  order_id = ${req.body.order_id}
      `
  db.query(sql, (err, results) => {
    if (err) throw err
    res.json(results)
  })
})

//ลบออเดอร์สินค้า
route.delete('/:id', (req, res) => {
  let sql = `
        DELETE FROM payment WHERE order_id = ${req.params.id};
      `
  db.query(sql)
  res.send(200)
})
module.exports = route